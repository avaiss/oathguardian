#!/bin/sh

source=oathguardian.svg

for size in 16 32 48 64 96 128 192 256 512 ; do
    mkdir -p "icons/${size}"
    inkscape -w ${size} -h ${size} -e "icons/${size}/oathguardian.png" ${source}
done


# coding=utf-8

# region License

# Copyright (c) 2017,2018 Alexandre Vaissière <avaiss@fmiw.org>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# endregion

import base64
import unittest

from oathguardian.oath import generate_totp, generate_hotp, new_default_formatter, new_steam_formatter


class RFC4246Tests(unittest.TestCase):
    """
    Tests values present in RFC4246.
    """
    @staticmethod
    def to_key(data: bytes) -> str:
        return base64.b32encode(data).decode('ascii')

    def setUp(self):
        self.key = self.to_key(b'12345678901234567890')

    def rfc_4246_test(self):
        test_values = (
            (0, 755224),
            (1, 287082),
            (2, 359152),
            (3, 969429),
            (4, 338314),
            (5, 254676),
            (6, 287922),
            (7, 162583),
            (8, 399871),
            (9, 520489),
        )

        for counter, hotp in test_values:
            self.assertEqual(hotp,
                             generate_hotp(self.key, counter, digits=6),
                             f"HOTP computation failed for {counter}")


class RFC6238Tests(unittest.TestCase):
    """
    Tests values present in RFC 6238.
    """
    @staticmethod
    def to_key(data: bytes) -> str:
        return base64.b32encode(data).decode('ascii')

    def setUp(self):
        self.keys = {
            'SHA1':   self.to_key(b'12345678901234567890'),
            'SHA256': self.to_key(b'12345678901234567890123456789012'),
            'SHA512': self.to_key(b'1234567890123456789012345678901234567890123456789012345678901234'),
        }
        self.period = 30

    def rfc_6238_test(self):
        test_values = (
            (59, '94287082', 'SHA1'),
            (59, '46119246', 'SHA256'),
            (59, '90693936', 'SHA512'),
            (1111111109, '07081804', 'SHA1'),
            (1111111109, '68084774', 'SHA256'),
            (1111111109, '25091201', 'SHA512'),
            (1111111111, '14050471', 'SHA1'),
            (1111111111, '67062674', 'SHA256'),
            (1111111111, '99943326', 'SHA512'),
            (1234567890, '89005924', 'SHA1'),
            (1234567890, '91819424', 'SHA256'),
            (1234567890, '93441116', 'SHA512'),
            (2000000000, '69279037', 'SHA1'),
            (2000000000, '90698825', 'SHA256'),
            (2000000000, '38618901', 'SHA512'),
            (20000000000, '65353130', 'SHA1'),
            (20000000000, '77737706', 'SHA256'),
            (20000000000, '47863826', 'SHA512'),
        )

        formatter = new_default_formatter(8)

        for timestamp, totp, mode in test_values:
            key = self.keys[mode]
            self.assertEqual(totp,
                             formatter(generate_totp(key, timestamp=timestamp, period=self.period, hash_algo=mode, digits=8)),
                             f"TOTP computation failed for {timestamp} with {mode}")


class SteamFormatterTests(unittest.TestCase):
    def test_steam(self):
        from ykman.oath import format_code
        import random

        formatter = new_steam_formatter()
        totp = random.Random().randint(0, 10**6 - 1)

        self.assertEqual(format_code(totp, digits=6, steam=True),
                         formatter(totp),
                         f"Invalid steam value for {totp}")

